#include "pch.h"
#include "Entity.h"

Entity::Entity()
{
    EntityType m_type = EntityType::none;
}

sf::FloatRect Entity::getBounds() {
    return {
            round(m_position.x),
            round(m_position.y),
            (float)m_size.x ,
            (float)m_size.y
    };
}

int Entity::getTop() {
	return round(m_position.y - m_size.y);
}

int Entity::getBottom() {
	return round(m_position.y);
}

int Entity::getLeft() {
	return round(m_position.x - m_size.y);
}