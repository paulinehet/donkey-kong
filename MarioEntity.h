#pragma once

#include "Entity.h"

class MarioEntity : public Entity
{
public:
	MarioEntity(sf::Vector2f);
	~MarioEntity();
public:
	void animation2img(std::string img1, std::string img2, int frameNum);
	void animation3img(std::string img1, std::string img2, std::string img3, int frameNum);
	void mountLadder(sf::Time elapsedTime);
	void climbUp(sf::Time elapsedTime);
	void updatePosition();
	void changePosition(int posX, int posY);
	void moveRightLeft(sf::Time elapsedTime);

private:
	void stopJumping();

public:
	bool mIsMovingUp;
	bool mIsMovingDown;
	bool mIsMovingRight;
	bool mIsMovingLeft;
	bool mIsFacingRight;
	bool mIsJumping;
	bool mIsJumpingHold;
	bool mIsClimbing;
	bool mIsUnderLadder;
	bool mIsAboveLadder;
	bool mIsOnTheFloor;
	bool mIsMountingLadder;
	bool mIsCollidingRight;
	bool mIsCollidingLeft;

	bool mIsClimbingUpAllowed;
	bool mIsClimbingDownAllowed;
	bool mIsJumpingAllowed;
	bool mIsWalkingLeftAllowed;
	bool mIsWalkingRightAllowed;
	
public:
	sf::Texture	m_Texture;

private:
	const float _playerSpeed = 100.f;

	int _jumpingFramesCounter = 0;
	int _movingFramesCounter = 0;
};

