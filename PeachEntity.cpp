#include "pch.h"
#include "PeachEntity.h"
#include "Game.h"
#include "EntityManager.h"

PeachEntity::PeachEntity(sf::Vector2f position)
{
	m_Texture.loadFromFile("Media/Textures/Peach.png");
	m_size = m_Texture.getSize();
	m_sprite.setTexture(m_Texture);
	m_sprite.setPosition(position.x, position.y - m_size.y);
	m_position = position;
	m_type = EntityType::peach;
}

PeachEntity::~PeachEntity() {}


