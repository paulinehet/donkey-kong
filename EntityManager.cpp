#include "pch.h"
#include "EntityManager.h"

std::vector<std::shared_ptr<Entity>> EntityManager::m_Entities;
std::shared_ptr<MarioEntity> EntityManager::_player;
std::shared_ptr<PeachEntity> EntityManager::_peach;

EntityManager::EntityManager()
{
}


EntityManager::~EntityManager()
{
}

void EntityManager::SetPlayer(std::shared_ptr<MarioEntity> player)
{
	EntityManager::_player = player;
}

std::shared_ptr<MarioEntity> EntityManager::GetPlayer()
{
	return EntityManager::_player;
}


void EntityManager::SetPeach(std::shared_ptr<PeachEntity> peach)
{
	EntityManager::_peach = peach;
}

std::shared_ptr<PeachEntity> EntityManager::GetPeach()
{
	return EntityManager::_peach;
}


std::vector<std::shared_ptr<Entity>> EntityManager::GetBarrels()
{
	std::vector<std::shared_ptr<Entity>> barrels;

	for (std::shared_ptr<Entity> entity : EntityManager::m_Entities)
	{
		if (entity->m_type == EntityType::barrel && entity->m_enabled == true)
		{
			barrels.push_back(entity);
		}
	}
	return barrels;
}

void EntityManager::Kill(std::shared_ptr<Entity> entity) {
	entity->m_enabled = false;
	m_Entities.erase(remove(m_Entities.begin(), m_Entities.end(), entity), m_Entities.end());
}