#pragma once

#include "MarioEntity.h"
#include "PeachEntity.h"

#define ECHELLE_COUNT 4
#define BLOCK_COUNT_X 10
#define BLOCK_COUNT_Y 5
#define BLOCK_SPACE 110.f
#define BARREL_COUNT_Y 4
#define BARREL_COUNT_X 1
#define DISPLAY_RES_X 1080
#define DISPLAY_RES_Y 600


class Game
{
public:
	Game();
	~Game() { };
	void run();

private:
	void processEvents();
	void update(sf::Time elapsedTime);
	void render();
	void drawMario();
	void drawLadders();
	void drawBlocks();
	void drawBarrels();
	void drawPlayArea();
	void drawPeach(sf::Vector2f pos);
	void updateStatistics(sf::Time elapsedTime);
	void updateBarrels();

	void updateMarioPermissions();
	void updateIsMarioClimbingUpAllowed();
	void updateIsMarioClimbingDownAllowed();
	void updateIsMarioJumpingAllowed();
	void updateIsMarioWalkingAllowed();

	void handlePlayerInput(sf::Keyboard::Key key, bool isPressed);
	void handleCollisionBarrelsPlayer();
	void handleTopCollisionLadder(); 
	void handleBottomCollisionLadder();
	void handleCollisionFloor();
	void handleCollisionMapBorders();
	void handleCollisionPeach();

private:
	static const sf::Time TimePerFrame;

	sf::RenderWindow mWindow;
	sf::Font mFont;
	sf::Font mWinLoseFont;
	sf::Text mStatisticsText;
	sf::Time mStatisticsUpdateTime;

	std::size_t	mStatisticsNumFrames;

	std::shared_ptr<MarioEntity> mario;
	std::shared_ptr<PeachEntity> peach;
	
	//Block
	sf::Texture	_TextureBlock;
	sf::Sprite _Block[BLOCK_COUNT_X][BLOCK_COUNT_Y];
	sf::Vector2u _sizeBlock;

	//Ladder
	sf::Texture	_TextureEchelle;
	sf::Sprite _Echelle[ECHELLE_COUNT];

	//Barrel
	sf::Texture	_TextureBarrel;
	sf::Sprite _Barrel[BARREL_COUNT_X][BARREL_COUNT_Y];
	sf::Vector2u _sizeBarrel;

	//PlayArea
	sf::Texture	_TexturePlayArea;
	sf::Sprite _PlayArea;
	sf::Vector2u _sizePlayArea;
	float _playAreaOffset;

	bool win;
	sf::Text winText;

	bool lose;
	sf::Text loseText;
};

