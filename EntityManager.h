#pragma once
#include "Entity.h"
#include "MarioEntity.h"
#include "PeachEntity.h"

class EntityManager
{
public:
	EntityManager();
	~EntityManager();

public:
	static std::vector<std::shared_ptr<Entity>> m_Entities;
	static void Kill(std::shared_ptr<Entity>);

	static std::shared_ptr<MarioEntity> GetPlayer();
	static void SetPlayer(std::shared_ptr<MarioEntity>);
	static std::shared_ptr<PeachEntity> GetPeach();
	static void SetPeach(std::shared_ptr<PeachEntity>);

	static std::vector<std::shared_ptr<Entity>> GetBarrels();

private:
	static std::shared_ptr<MarioEntity> _player;
	static std::shared_ptr<PeachEntity> _peach;
};

