#pragma once

enum class EntityType
{
	none,
	player,
	block,
	ladder,
	barrel,
	playArea,
	peach
};

class Entity
{
public:
	Entity();
	virtual ~Entity() { };
public:
	sf::FloatRect getBounds();
	int getTop();
	int getBottom();
	int getLeft();
	int getRight();
public:
	sf::Sprite m_sprite;
	sf::Vector2u m_size;
	sf::Vector2f m_position;
	EntityType m_type = EntityType::none;
	bool m_enabled = true;

	// Barrel only
	bool m_goesRight = true;
	int m_times = 0;
};

