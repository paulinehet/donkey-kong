#pragma once

#include "Entity.h"

class PeachEntity : public Entity
{
public:
	PeachEntity(sf::Vector2f);
	~PeachEntity();
	
public:
	sf::Texture	m_Texture;
};

