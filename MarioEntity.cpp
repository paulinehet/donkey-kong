#include "pch.h"
#include "MarioEntity.h"
#include "Game.h"
#include "EntityManager.h"

const float _playerSpeed = 100.f;

MarioEntity::MarioEntity(sf::Vector2f position) :
	mIsMovingUp(false),
	mIsMovingDown(false),
	mIsMovingRight(false),
	mIsMovingLeft(false),
	mIsFacingRight(true),
	mIsJumping(false),
	mIsJumpingHold(false),
	mIsClimbing(false),
	mIsOnTheFloor(true),
	mIsAboveLadder(false),
	mIsUnderLadder(false),
	mIsMountingLadder(false),
	mIsCollidingLeft(false),
	mIsCollidingRight(false),
	mIsClimbingUpAllowed(false),
	mIsClimbingDownAllowed(false),
	mIsJumpingAllowed(true),
	mIsWalkingLeftAllowed(true),
	mIsWalkingRightAllowed(true)
	
{
	m_Texture.loadFromFile("Media/Textures/MarioIdleRight.png");
	m_size = m_Texture.getSize();
	m_sprite.setTexture(m_Texture);
	m_sprite.setPosition(position.x, position.y - m_size.y);
	m_position = position;
	m_type = EntityType::player;
	
}

MarioEntity::~MarioEntity(){}

void MarioEntity::animation2img(std::string img1, std::string img2, int frameNum) {
	if (_movingFramesCounter < frameNum/2) {
		m_Texture.loadFromFile(img1);
	}
	else if (_movingFramesCounter >= frameNum/2) {
		m_Texture.loadFromFile(img2);
	}
	_movingFramesCounter += 1;

	if (_movingFramesCounter == frameNum) {
		_movingFramesCounter = 0;
	}
}

void MarioEntity::animation3img(std::string img1, std::string img2, std::string img3, int frameNum) {
	if (_movingFramesCounter < frameNum * 0.33) {
		m_Texture.loadFromFile(img1);
	}
	else if (_movingFramesCounter >= frameNum * 0.33 && _movingFramesCounter < frameNum * 0.66) {
		m_Texture.loadFromFile(img2);
	}
	else if (_movingFramesCounter >= frameNum * 0.66 && _movingFramesCounter < frameNum) {
		m_Texture.loadFromFile(img3);
	}
	_movingFramesCounter += 1;

	if (_movingFramesCounter == frameNum) {
		_movingFramesCounter = 0;
		m_Texture.loadFromFile("Media/Textures/MarioIdleRight.png");
		m_size = m_Texture.getSize();
	}
}

void MarioEntity::mountLadder(sf::Time elapsedTime)
{

	animation3img("Media/Textures/MarioMounting1.png", "Media/Textures/MarioMounting2.png", "Media/Textures/MarioMounting3.png", 90);
	
	if (_movingFramesCounter == 0) {
		mIsOnTheFloor = true;
		mIsMountingLadder = false;
	}

	
}

void MarioEntity::climbUp(sf::Time elapsedTime)
{
	sf::Vector2f movement(0.f, 0.f);
	
	
	if (mIsClimbingUpAllowed && mIsMovingUp)
	{
		stopJumping();
		mIsClimbing = true;
		animation2img("Media/Textures/MarioClimbing1.png", "Media/Textures/MarioClimbing2.png", 10);
		movement.y -= _playerSpeed;
	}
	if (mIsClimbingDownAllowed && mIsMovingDown)
	{
		stopJumping();
		mIsClimbing = true;
		animation2img("Media/Textures/MarioClimbing2.png", "Media/Textures/MarioClimbing1.png", 10);
		movement.y += _playerSpeed;
	}
	std::shared_ptr<Entity> player = EntityManager::GetPlayer();
	if (player != nullptr) {
		player->m_sprite.move(movement * elapsedTime.asSeconds());
		updatePosition();
	}
}

void MarioEntity::updatePosition()
{
	m_position = sf::Vector2f(round(m_sprite.getPosition().x + m_size.x), round(m_sprite.getPosition().y + m_size.y));
}

void MarioEntity::changePosition(int posX, int posY)
{
	m_sprite.setPosition(posX, posY);
	updatePosition();
}

void MarioEntity::moveRightLeft(sf::Time elapsedTime)
{
	sf::Vector2f movement(0.f, 0.f);
	mIsClimbing = false;
	
	if (mIsWalkingLeftAllowed && mIsMovingLeft) {
		if (mIsOnTheFloor)
			animation2img("Media/Textures/MarioWalkLeft1.png", "Media/Textures/MarioWalkLeft2.png", 10);
		movement.x -= _playerSpeed;
		mIsFacingRight = false;
	}

	if (mIsWalkingRightAllowed && mIsMovingRight) {
		if (mIsOnTheFloor)
			animation2img("Media/Textures/MarioWalkRight1.png", "Media/Textures/MarioWalkRight2.png", 10);
		mIsFacingRight = true;
		movement.x += _playerSpeed;
	}
		
	if (mIsJumping) {
		if (_jumpingFramesCounter < 35) {
			movement.y -= _playerSpeed * 1;
		}
		else if (_jumpingFramesCounter >= 35) {
			movement.y += _playerSpeed * 1;
		}
		_jumpingFramesCounter += 1;

		if (_jumpingFramesCounter == 70) {
			stopJumping();
		}
	}

	std::shared_ptr<Entity> player = EntityManager::GetPlayer();
	if (player != nullptr) {
		player->m_sprite.move(movement * elapsedTime.asSeconds());
		updatePosition();
	}
}


void MarioEntity::stopJumping() {
	mIsJumping = false;
	_jumpingFramesCounter = 0;
}
