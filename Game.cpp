#include "pch.h"
#include "StringHelpers.h"
#include "Game.h"
#include "EntityManager.h"


const float _fps = 60.f;
const sf::Time Game::TimePerFrame = sf::seconds(1.f / _fps); // 60 frame par seconde
const auto GameFontPath = "Media/Fonts/DonkeyKong.ttf";

Game::Game() :
	mWindow(sf::VideoMode(DISPLAY_RES_X, DISPLAY_RES_Y), "Donkey Kong 1981", sf::Style::Close),
	mFont(),
	mStatisticsText(),
    mStatisticsUpdateTime(),
	mStatisticsNumFrames(0),
	win(false),
	lose(false)
{
	mWindow.setFramerateLimit(160);
	
	drawBlocks();
	drawLadders();
	drawMario();
	drawBarrels();
	drawPlayArea();

	// Draw Statistic Font 

	mFont.loadFromFile("Media/Fonts/Sansation.ttf");
	mStatisticsText.setString("Welcome to Donkey Kong 1981");
	mStatisticsText.setFont(mFont);
	mStatisticsText.setPosition(5.f, 5.f);
	mStatisticsText.setCharacterSize(10);
}

void Game::drawMario()
{
	// Draw Mario

	sf::Vector2f position = sf::Vector2f(_playAreaOffset, BLOCK_SPACE * 5);
	mario = std::make_shared<MarioEntity>(position);
	EntityManager::SetPlayer(mario);
}

void Game::drawBlocks()
{
	// Draw blocks

	_TextureBlock.loadFromFile("Media/Textures/Block.png");
	_sizeBlock = _TextureBlock.getSize();
	std::cout << "Block Size: x:" << _sizeBlock.x << " y:" << _sizeBlock.y << std::endl;
	
	_playAreaOffset = (DISPLAY_RES_X / 2.f) - (_sizeBlock.x * BLOCK_COUNT_X / 2.f);

	for (int i = 0; i < BLOCK_COUNT_X; i++)
	{
		for (int j = 0; j < BLOCK_COUNT_Y; j++)
		{
			_Block[i][j].setTexture(_TextureBlock);
			_Block[i][j].setPosition(_playAreaOffset + (_sizeBlock.x * i), 0.f + BLOCK_SPACE * (j + 1));

			std::shared_ptr<Entity> se = std::make_shared<Entity>();
			se->m_sprite = _Block[i][j];
			se->m_type = EntityType::block;
			se->m_size = _sizeBlock;
			se->m_position = _Block[i][j].getPosition();
			EntityManager::m_Entities.push_back(se);

			if (i == BLOCK_COUNT_X-1 && j == 0) {
				drawPeach(_Block[i][j].getPosition());
			}
		}
	}
}

void Game::drawLadders()
{
	// Draw Echelles
	_TextureEchelle.loadFromFile("Media/Textures/Echelle.png");

	for (int i = 0; i < ECHELLE_COUNT; i++)
	{
		_Echelle[i].setTexture(_TextureEchelle);
		_Echelle[i].setPosition(_playAreaOffset + _sizeBlock.x * i , 0.f + BLOCK_SPACE * (i + 1) + _sizeBlock.y);

		std::shared_ptr<Entity> se = std::make_shared<Entity>();
		se->m_sprite = _Echelle[i];
		se->m_type = EntityType::ladder;
		se->m_size = _TextureEchelle.getSize();
		se->m_position = _Echelle[i].getPosition();
		EntityManager::m_Entities.push_back(se);
	}
}

void Game::drawBarrels()
{
	// Draw barrels

	_TextureBarrel.loadFromFile("Media/Textures/Barrel.png");
	_sizeBarrel = _TextureBarrel.getSize();
	std::cout << "Barrel Size: x:" << _sizeBarrel.x << " y:" << _sizeBarrel.y << std::endl;

	for (int i = 0; i < BARREL_COUNT_X; i++)
	{
		for (int j = 0; j < BARREL_COUNT_Y; j++)
		{
			sf::Vector2f posBarrel;


			_Barrel[i][j].setTexture(_TextureBarrel);

			posBarrel.x = _playAreaOffset + _sizeBlock.x;
			posBarrel.y = BLOCK_SPACE * (j + 1) - _sizeBarrel.y;
			_Barrel[i][j].setPosition(posBarrel);

			std::shared_ptr<Entity> barrel = std::make_shared<Entity>();
			barrel->m_sprite = _Barrel[i][j];
			barrel->m_type = EntityType::barrel;
			barrel->m_size = _TextureBarrel.getSize();
			barrel->m_position = _Barrel[i][j].getPosition();
			barrel->m_goesRight = true;
			EntityManager::m_Entities.push_back(barrel);
		}
	}
}

void Game::drawPlayArea()
{	
	_sizePlayArea.y = (_sizeBlock.y + BLOCK_SPACE) * BLOCK_COUNT_Y;
	_sizePlayArea.x = _sizeBlock.x * BLOCK_COUNT_X;

	_PlayArea.setPosition(_playAreaOffset, 0);

	std::shared_ptr<Entity> playArea = std::make_shared<Entity>();
	playArea->m_sprite = _PlayArea;
	playArea->m_type = EntityType::playArea;
	playArea->m_size = _sizePlayArea;
	playArea->m_position = _PlayArea.getPosition();
	EntityManager::m_Entities.push_back(playArea);
}


void Game::drawPeach(sf::Vector2f pos)
{
	// Draw Peach

	sf::Vector2f position = pos;
	peach = std::make_shared<PeachEntity>(position);
	EntityManager::SetPeach(peach);

}

void Game::run()
{
	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;
	while (mWindow.isOpen())// boucle du jeu 
	{
		sf::Time elapsedTime = clock.restart();// remet le chrono � 0, return le temps ecoul� depuis le dernier restart
		timeSinceLastUpdate += elapsedTime;

		while (timeSinceLastUpdate > TimePerFrame)//quand la fps est ecoul� on rentre dedans 
		{
			timeSinceLastUpdate -= TimePerFrame; // on enleve du temps 

			processEvents(); //enregistre la saisie utilisateur
			update(TimePerFrame); // bouge mario la position, mais pas visuellement
		}

		updateStatistics(elapsedTime);// gestion des collision
		render(); //afficher les textures
		// sleep 1/fps
	}
}

void Game::render()
{
	mWindow.clear();

	if (win || lose) {
		mWinLoseFont.loadFromFile("Media/Fonts/WinLoseFont.ttf");
		winText.setString(win ? "You won !" : "You lose");
		winText.setFont(mWinLoseFont);
		winText.setPosition(_playAreaOffset + (_sizePlayArea.x/2) - 60, (_sizePlayArea.y / 2) - 150);
		winText.setCharacterSize(100);
		winText.setFillColor(sf::Color::White);

		mWindow.draw(winText);
		mWindow.draw(mStatisticsText);

	} else {
		for (std::shared_ptr<Entity> entity : EntityManager::m_Entities)
		{
			if (entity->m_enabled == false)
			{
				continue;
			}

			mWindow.draw(entity->m_sprite);
		}

		mWindow.draw(mario->m_sprite);
		mWindow.draw(peach->m_sprite);
		mWindow.draw(mStatisticsText);
	}
	
	mWindow.display();
}

void Game::processEvents()
{
	sf::Event event;
	while (mWindow.pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::KeyPressed:
			handlePlayerInput(event.key.code, true);
			break;

		case sf::Event::KeyReleased:
			handlePlayerInput(event.key.code, false);
			break;

		case sf::Event::Closed:
			mWindow.close();
			break;
		}
	}
}

void Game::handlePlayerInput(sf::Keyboard::Key key, bool isPressed)
{
	if (key == sf::Keyboard::Up) {
		mario->mIsMovingUp = isPressed;
	}
	else if (key == sf::Keyboard::Down) {
		mario->mIsMovingDown = isPressed;
	}
	else if (key == sf::Keyboard::Left) {
		mario->mIsMovingLeft = isPressed;
	}
	else if (key == sf::Keyboard::Right) {
		mario->mIsMovingRight = isPressed;
	}
	else if (key == sf::Keyboard::Space && isPressed == false) {
		mario->mIsJumpingHold = false;
	}
	else if (key == sf::Keyboard::Space && isPressed == true && mario->mIsJumpingHold == false)
	{
		if (mario->mIsJumping == true) // Si mario saute d�j�
		{
			return;
		}

		if (mario->mIsJumpingAllowed) {
			mario->mIsJumping = true;
			mario->mIsOnTheFloor = false;
			mario->mIsJumpingHold = true;
		}

	}
}

void Game::update(sf::Time elapsedTime)
{
	updateMarioPermissions();

	if (mario->mIsMountingLadder) mario->mountLadder(elapsedTime);
	mario->climbUp(elapsedTime);

	if (mario->mIsWalkingLeftAllowed || mario->mIsWalkingRightAllowed) {
		
		if (mario->mIsFacingRight)
			mario->mIsOnTheFloor ? mario->m_Texture.loadFromFile("Media/Textures/MarioIdleRight.png") : mario->m_Texture.loadFromFile("Media/Textures/MarioJumpRight.png");
		else
			mario->mIsOnTheFloor ? mario->m_Texture.loadFromFile("Media/Textures/MarioIdleLeft.png") : mario->m_Texture.loadFromFile("Media/Textures/MarioJumpLeft.png");
	}

	mario->moveRightLeft(elapsedTime);
	
	updateBarrels();
	handleCollisionBarrelsPlayer();
	handleTopCollisionLadder();
	handleBottomCollisionLadder();
	handleCollisionFloor();
	handleCollisionMapBorders();
	handleCollisionPeach();
}

void Game::updateBarrels() {
	std::vector<std::shared_ptr<Entity>> barrels = EntityManager::GetBarrels();
	
	for (std::shared_ptr<Entity> barrel : barrels)
	{
		sf::Vector2f barrelPosition = barrel->m_sprite.getPosition();

		if (barrelPosition.x + _sizeBarrel.x + 1 >= _PlayArea.getPosition().x + _sizePlayArea.x)
		{
			barrel->m_goesRight = false;
		}
		else if (barrelPosition.x - 1 < _playAreaOffset) {
			barrel->m_goesRight = true;
		}

		if (barrel->m_goesRight == true)
			barrelPosition.x += 2;
		else
			barrelPosition.x -= 2;

		barrel->m_sprite.setPosition(barrelPosition);
	}
}

void Game::updateStatistics(sf::Time elapsedTime)
{
	mStatisticsUpdateTime += elapsedTime;
	mStatisticsNumFrames += 1;

	if (mStatisticsUpdateTime >= sf::seconds(1.0f)) // Toutes les 1 secondes
	{
		mStatisticsText.setString(
			"Frames / Second = " + toString(mStatisticsNumFrames) + "\n" +
			"Time / Update = " + toString(mStatisticsUpdateTime.asMicroseconds() / mStatisticsNumFrames) + "us");

		mStatisticsUpdateTime -= sf::seconds(1.0f);
		mStatisticsNumFrames = 0;
	}
}

/*
IsClimbingUpAllowed :
- If (mario.top is) BehindLadder
- !If Jumping
*/
void Game::updateIsMarioClimbingUpAllowed() {
	mario->mIsClimbingUpAllowed = mario->mIsUnderLadder && !mario->mIsMountingLadder;
}

/*
IsClimbingDownAllowed :
- If (mario.bottom +1 is) BehindLadder
- !If Jumping
*/
void Game::updateIsMarioClimbingDownAllowed() {
	mario->mIsClimbingDownAllowed = ((mario->mIsAboveLadder && mario->mIsOnTheFloor) || (!mario->mIsOnTheFloor && mario->mIsUnderLadder)) && !mario->mIsMountingLadder;
}

/*
IsJumpingAllowed :
- If OnTheFloor or
- If IsClimbing && OnTheFloor
- If IsMountingLadder
*/
void Game::updateIsMarioJumpingAllowed() {
	mario->mIsJumpingAllowed = mario->mIsOnTheFloor && !mario->mIsMountingLadder;
}

/*
IsWalkingAllowed :
- If OnTheFloor
- If !IsMountingLadder
*/
void Game::updateIsMarioWalkingAllowed() {
	mario->mIsWalkingLeftAllowed = (mario->mIsOnTheFloor || mario->mIsJumping) && !mario->mIsMountingLadder && !mario->mIsCollidingLeft;
	mario->mIsWalkingRightAllowed = (mario->mIsOnTheFloor || mario->mIsJumping) && !mario->mIsMountingLadder && !mario->mIsCollidingRight;
}

void Game::updateMarioPermissions() {
	updateIsMarioClimbingUpAllowed();
	updateIsMarioClimbingDownAllowed();
	updateIsMarioWalkingAllowed();
	updateIsMarioJumpingAllowed();
}

void Game::handleCollisionBarrelsPlayer() {
	std::vector<std::shared_ptr<Entity>> barrels = EntityManager::GetBarrels();
	sf::FloatRect marioBounds = mario->m_sprite.getGlobalBounds();
	for (std::shared_ptr<Entity> barrel : barrels)
	{
		sf::FloatRect barrelBounds = barrel->m_sprite.getGlobalBounds();
		if (barrelBounds.intersects(marioBounds)) {
			
			EntityManager::Kill(barrel);
			lose = true;
		}
	}
}

void Game::handleTopCollisionLadder() {
	mario->mIsUnderLadder = false;

	for (std::shared_ptr<Entity> entity : EntityManager::m_Entities) {
		if (entity->m_enabled == false)
		{
			continue;
		}

		if (entity->m_type != EntityType::ladder)
		{
			continue;
		}

		sf::FloatRect boundLadder;
		boundLadder = entity->m_sprite.getGlobalBounds();

		sf::FloatRect boundPlayer;
		boundPlayer = mario->m_sprite.getGlobalBounds();
		
		if (boundPlayer.intersects(boundLadder) && mario->getTop() >= boundLadder.top && mario->getTop() <= boundLadder.top + boundLadder.height) {
			mario->mIsUnderLadder = true;
			if (!mario->mIsJumping && mario->getTop() <= boundLadder.top) {
				mario->mIsMountingLadder = true;
				mario->changePosition(mario->m_sprite.getPosition().x, boundLadder.top - _sizeBlock.y - mario->m_size.y);
			}
			break;
		}
	}
}

void Game::handleBottomCollisionLadder() {
	mario->mIsAboveLadder = false;
	for (std::shared_ptr<Entity> entity : EntityManager::m_Entities) {
		if (entity->m_enabled == false)
		{
			continue;
		}

		if (entity->m_type != EntityType::ladder)
		{
			continue;
		}

		sf::FloatRect boundLadder;
		boundLadder = entity->m_sprite.getGlobalBounds();
		boundLadder.top -= _sizeBlock.y;
		boundLadder.height += _sizeBlock.y; 

		sf::FloatRect boundPlayer;
		boundPlayer = mario->m_sprite.getGlobalBounds();
		boundPlayer.height += 1;

		if (boundPlayer.intersects(boundLadder) && mario->getBottom() >= boundLadder.top && mario->getBottom() <= boundLadder.top + boundLadder.height) {
			mario->mIsAboveLadder = true;
			if (!mario->mIsJumping && mario->getBottom() >= boundLadder.top + 1 && mario->getBottom() < boundLadder.top + _sizeBlock.y) {
				mario->changePosition(mario->m_sprite.getPosition().x, boundLadder.top + _sizeBlock.y);
			}
			break;
		}
	}
}

void Game::handleCollisionFloor() {
	if (mario->mIsJumping) {
		return;
	}


	for (std::shared_ptr<Entity> entity : EntityManager::m_Entities) {
		if (entity->m_enabled == false)
		{
			continue;
		}

		if (entity->m_type != EntityType::block)
		{
			continue;
		}

		sf::FloatRect boundFloor;
		boundFloor = entity->m_sprite.getGlobalBounds();

		sf::FloatRect boundPlayer;
		boundPlayer = mario->m_sprite.getGlobalBounds();
		boundPlayer.height += 1;

		if (boundFloor.intersects(boundPlayer) && mario->m_position.y == boundFloor.top) {
			mario->mIsOnTheFloor = true;
			break;
		}
		else {
			mario->mIsOnTheFloor = false;
		}
	}
}


void Game::handleCollisionMapBorders() {
	mario->mIsCollidingLeft = false;
	mario->mIsCollidingRight = false;
	if (mario->m_position.x - mario->m_size.x <= _PlayArea.getPosition().x) {
		mario->mIsCollidingLeft = true;
	}
	else if (mario->m_position.x >= _sizePlayArea.x +  _PlayArea.getPosition().x){
		mario->mIsCollidingRight = true;
	}
}

void Game::handleCollisionPeach() {
	sf::FloatRect boundPeach;
	boundPeach = peach->m_sprite.getGlobalBounds();

	sf::FloatRect boundPlayer;
	boundPlayer = mario->m_sprite.getGlobalBounds();

	if (boundPeach.intersects(boundPlayer)) {
		win = true;
	}
}